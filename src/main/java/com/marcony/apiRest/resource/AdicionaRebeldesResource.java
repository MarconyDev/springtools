package com.marcony.apiRest.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.marcony.apiRest.model.AdicionaRebeldes;
import com.marcony.apiRest.repository.AdicionarRebeldes;

import java.util.List;

@RestController
@RequestMapping("/AdicionarRebeldes")
public class AdicionaRebeldesResource {
	
	@Autowired
	private AdicionarRebeldes  adicionarRebeldes;
	
	@PostMapping
	public AdicionaRebeldes adicionar (@RequestBody AdicionaRebeldes rebelde) {
		return adicionarRebeldes.save(rebelde);
	}
	
	public List<AdicionaRebeldes> listar(){
		return adicionarRebeldes.findAll() ;
	}
}
